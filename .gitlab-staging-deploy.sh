#!/bin/bash
# Ensure we fail fast if there is a problem.
#set -eo pipefail

# Setup Git remote
git remote add "$GIT_DEPLOYMENT_REMOTE" "$GIT_DEPLOYMENT_URL"
# Deploy via Git
git push "$GIT_DEPLOYMENT_REMOTE" HEAD:"$GIT_DEPLOYMENT_BRANCH"

curl -n -X POST https://api.heroku.com/apps/arcane-sierra-98225/dynos -H "Accept: application/vnd.heroku+json; version=3" -H "Content-Type: application/json" -H "Authorization: Bearer 2bd50337-d6d1-4051-b2c0-a509f898e212" -d "{ \"attach\":false, \"env\": { \"APP_ENV\":\"local\"}, \"command\":\"php artisan migrate:refresh\" } "
